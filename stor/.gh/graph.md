# FS-CACHE
https://blog.frehi.be/2019/01/03/fs-cache-for-nfs-clients/

# NFS
## NFS caching
[sch](https://www.google.com/search?q=nfs+cache)

https://www.avidandrew.com/understanding-nfs-caching.html

https://www.youtube.com/results?search_query=nfs+ganesha

## NFS-Ganesha support for GlusterFS
https://youtu.be/Sf9Ue-TOets

# Performance
https://photographylife.com/afp-vs-nfs-vs-smb-performance

# AFP
https://wiki.archlinux.org/index.php/Netatalk
